<?php

namespace App\Model;

class Currency {
    /** @var string */
    public $ID;

    /** @var string */
    public $NumCode;

    /** @var string */
    public $CharCode;

    /** @var int */
    public $Nominal;

    /** @var string */
    public $Name;

    /** @var float */
    public $Value;

    /** @var float */
    public $Previous;
}