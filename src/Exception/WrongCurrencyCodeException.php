<?php

namespace App\Exception;

use Throwable;

class WrongCurrencyCodeException extends \Exception {

    /** @var string */
    private $currencyCode;

    /**
     * @return string
     */
    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode(string $currencyCode)
    {
        $this->currencyCode = $currencyCode;
    }
}