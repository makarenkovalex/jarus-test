<?php

namespace App\Controller;

use App\Service\Currency\Fetcher;
use App\Service\Currency\Formatter;
use App\Exception\WrongCurrencyCodeException;
use App\Service\Currency\Extractor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
Use Symfony\Component\Routing\Annotation\Route;

class ExchangeController extends AbstractController
{
    /** @var string */
    private $currencyUrl;

    /** @var Extractor */
    private $extractor;

    /** @var Formatter */
    private $formatter;
    /**
     * @var Fetcher
     */
    private $fetcher;

    public function __construct(Extractor $extractor, Formatter $formatter, Fetcher $fetcher, string $currencyUrl)
    {
        $this->extractor = $extractor;
        $this->formatter = $formatter;
        $this->currencyUrl = $currencyUrl;
        $this->fetcher = $fetcher;
    }

    /**
    * @Route("/exchange/{currencyCode}", name="exchange_currency")
    */
    public function exchangeCurrency(string $currencyCode): Response
    {
        try {
            $currency = $this->extractor->extract(file_get_contents($this->currencyUrl), $currencyCode);
            $rubleFormatted = $this->formatter->format($currency->Value);
            $message = "{$currency->Nominal} {$currency->Name} равен {$currency->Value} $rubleFormatted";
            return new JsonResponse([$currencyCode => $message]);
        } catch (WrongCurrencyCodeException $e) {
            return new Response("Код валюты {$e->getCurrencyCode()} не найден", Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return new Response('Произошла ошибка - ' . $e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
    * @Route("/exchange", name="exchange_random_currency")
    */
    public function exchangeRandomCurrency(): Response
    {
        try {
            $currency = $this->fetcher->getRandom();
            $rubleFormatted = $this->formatter->format($currency->Value);
            $message = "{$currency->Nominal} {$currency->Name} равен {$currency->Value} $rubleFormatted";
            return new JsonResponse([$currency->CharCode => $message]);
        } catch (WrongCurrencyCodeException $e) {
            return new Response("Код валюты {$e->getCurrencyCode()} не найден", Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return new Response('Произошла ошибка - ' . $e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }
}