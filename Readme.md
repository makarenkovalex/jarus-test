## Install
[Install docker on your machine.][install-docker]

[Install docker-compose on your machine.][install-docker-compose]

Clone this repository.

``` bash
$ git clone git@gitlab.com:makarenkovalex/jarus-test.git
```

Switch to the cloned directory.

``` bash
$ cd jarus-test
```

Start the stack.

``` bash
$ docker-compose up
```

[install-docker]: https://docs.docker.com/engine/installation
[install-docker-compose]: https://docs.docker.com/compose/install
