FROM php:7.4-fpm
WORKDIR /app

RUN apt-get update -y && apt-get install -y \
    && apt-get autoremove -y \
    && docker-php-ext-install mysqli pdo pdo_mysql \
    && apt-get install curl -y \
    && apt-get install git -y\
    && apt-get install zip -y


RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN curl -sS https://get.symfony.com/cli/installer | bash
RUN mv /root/.symfony/bin/symfony /usr/local/bin/symfony

COPY . .

RUN composer install
